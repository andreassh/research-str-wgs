find_coverage = {
	exec """
		samtools depth $input.bam | awk '{sum+=\$3} END { print sum/NR}' > downsampled/$output.txt
	""", "quick"
}

def get_averagecoverage(file) {
        samplename = file.split('/')[-1].split('\\.')[0]
        coverage = new File(samplename + ".merge.dedup.coverage.txt").text
        return(coverage)
}

downsample = {
	def lowestcov = 29.3761 as Double
        def coverage = get_averagecoverage(input) as Double
	def ratio = lowestcov / coverage as Double

	if (ratio != 0) {
		exec """
		   gatk DownsampleSam \
		       -I $input.bam \
		       -O $output.bam \
		       -P $ratio
		""", "bamm"
	}
}

index = {
	exec """
		samtools index $input.bam
	""", "quick"
}

qualimap = {
	exec """
		./soft/qualimap_v2.2.1/qualimap --java-mem-size=32000M bamqc \
		-bam $input.bam \
		-outdir ./results/qualimap \
		-outformat pdf \
		-outfile $output.pdf
	""", "qualimap"
}
run {
	~'(.*)?.bam' * [
		find_coverage + downsample + index + qualimap
	]
}
