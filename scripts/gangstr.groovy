REFERENCE_GENOME_HG19 = './reference/hg19.fa'
TRF_HG19 = './reference/TRF/hg19_2.7.7.80.10.50.6_gangstr_2-6.bed'

gangstr = {
	exec """
		LD_LIBRARY_PATH=./soft/gcc/lib64 \
		./soft/gangstr/bin/GangSTR \
	 	--bam $input.bam \
        --ref $REFERENCE_GENOME_HG19 \
        --regions $TRF_HG19 \
        --out $output \
        --quiet
	""", "gangstr_wgs"
}

run {
	~'(.*)?.bam' * [
		gangstr
	]
}
