# Extract data from GangSTR
# Example usage: extract_data_gangstr.py --vcf /folder/to/vcf [--out combine/Y]

import os
import pysam
import csv
import re
import regex
import pybedtools
from argparse import ArgumentParser

def get_strs(vcf_file, output, sample):
    with open(vcf_file) as csvfile:
        readCSV = csv.reader(csvfile, delimiter='\t')

        for row in readCSV:
            # Discard the lines that start with #
            if row[0].startswith('#') or row[9] == ".":
                continue

            # Define variables
            chr = 'chr' + row[0] if row[0].isnumeric() else row[0] # Chromosome name
            STR_start_pos = int(row[1])
            STR_end_pos = int(re.search(r'END=(.*);RU', row[7]).group(1))  # Find STR end position from 8th column between "END=" and ";RU"
            ref_STR_sequence = row[3]
            motif = re.search(r'RU=(.*);PERIOD', row[7]).group(1)  # Find motif from 8th column between "RU=" and ";PERIOD" -  GangSTR
            ref_len = int(re.search(r'REF=(.*);GRID', row[7]).group(1))
            STR_coverage_enclosing = row[9].split(":")[5].split(",")[0]
            STR_coverage_FRR = row[9].split(":")[5].split(",")[2]
            STR_coverage_spanning = row[9].split(":")[5].split(",")[1]
            STR_coverage_flanking = row[9].split(":")[5].split(",")[3]
            CI = row[9].split(":")[4]
            quality_score = row[9].split(":")[2]
            filter_pass = 'PASS'

            if int(STR_coverage_enclosing+STR_coverage_spanning+STR_coverage_FRR+STR_coverage_flanking) == 0:
                continue

            if int(STR_coverage_enclosing+STR_coverage_spanning) < 1:
                filter_pass = 'BADCOV'

            detected_len = row[9].split(":")[3]
            alleles = re.search(r'(.*):', str(row[9])).group(1)[0:3] # Get the genotype code from the 10th column
            detected_sequence = row[4]

            # Type of alleles (either same or different from the reference)
            allele1_num = int(alleles.split('/')[0])
            allele2_num = int(alleles.split('/')[1])

            if allele1_num == 0:
                detected_sequence_allele1 = ref_STR_sequence
            elif allele1_num >= 1:
                detected_sequence_allele1 = row[4].split(",")[allele1_num - 1] # -1 because of python counting
            else:
                detected_sequence_allele1 = 'NA'

            if allele2_num == 0:
                detected_sequence_allele2 = ref_STR_sequence
            elif allele2_num >= 1:
                detected_sequence_allele2 = row[4].split(",")[allele2_num - 1] # -1 because of python counting
            else:
                detected_sequence_allele2 = 'NA'

            
            # Check confidence intervals and if the result is not in the CI then write into FilterPass field BADCI
            if re.search(r',', detected_len):
                detected_len_allele1 = int(detected_len.split(",")[0])
                detected_len_allele2 = int(detected_len.split(",")[1])

                CI_allele1 = CI.split(",")[0]
                CI_allele2 = CI.split(",")[1]
                CI_allele1_low = int(CI_allele1.split("-")[0])
                CI_allele1_high = int(CI_allele1.split("-")[1])

                CI_allele2_low = int(CI_allele2.split("-")[0])
                CI_allele2_high = int(CI_allele2.split("-")[1])

                if detected_len_allele1 < CI_allele1_low or detected_len_allele1 > CI_allele1_high:
                    filter_pass += ',' if filter_pass != "PASS" else ""
                    filter_pass += 'BADCIA1'

                if detected_len_allele2 < CI_allele2_low or detected_len_allele2 > CI_allele2_high:
                    filter_pass += ',' if filter_pass != "PASS" else ""
                    filter_pass += 'BADCIA2'

            else:
                detected_len_allele1 = int(detected_len)
                detected_len_allele2 = 'NA'
                CI_allele1 = CI
                CI_allele1_low = int(CI_allele1.split("-")[0])
                CI_allele1_high = int(CI_allele1.split("-")[1])

                if detected_len_allele1 < CI_allele1_low or detected_len_allele1 > CI_allele1_high:
                    filter_pass += ',' if filter_pass != "PASS" else ""
                    filter_pass += 'BADCI'


            # Determine whether it is homozygous or heterozygous
            if detected_len_allele1 == detected_len_allele2:
                genotype = 'HOM'
            else:
                genotype = 'HET'

            # Print data out
            out = (
                str(sample) + '\t' + # Sample name
                str(chr) + ':' + str(STR_start_pos) + '-' + str(STR_end_pos) + '\t' + # Locus
                str(chr) + '\t' + # Chromosome
                str(STR_start_pos) + '\t' + # STR locus start position
                str(STR_end_pos) + '\t' + # STR locus end position
                str(len(motif)) + '\t' + # Motif length
                str(motif.upper()) + '\t' + # Motif
                str(ref_len) + '\t' + # Reference length
                str(detected_len_allele1) + '\t' +
                str(detected_len_allele2) + '\t' +
                str(alleles) + '\t' + # Alleles
                str(genotype) + '\t' + # Genotype
                str(STR_coverage_enclosing) + '\t' + 
                str(STR_coverage_FRR) + '\t' + 
                str(STR_coverage_spanning) + '\t' + 
                str(STR_coverage_flanking) + '\t' + 
                str(CI_allele1) + '\t' +
                str(CI_allele2) + '\t' +
                str(quality_score) + '\t' +
                str(filter_pass) + '\n'
            )

            if output is not None:  # If user has set output file then write file, else print text on screen
                output.write(out)
            else:
                print(out)

def main():
    parser = ArgumentParser()
    parser.add_argument("-v", "--vcf", dest="vcf_file", required=True, help="VCF file path")
    parser.add_argument("-o", "--out", dest="writefile", required=False, choices=['combine', "single"], help="Write an output file?")

    args = parser.parse_args()

    writefile = args.writefile

    if args.vcf_file:
        vcf_files_path = args.vcf_file
    else:
        vcf_files_path = './raw_vcf/'
        output_files_path = vcf_files_path + '../sorted_txt/'

    output_files_path = './'

    header = 'Sample\tLocus\tChromosome\tReadStart\tReadEnd\tMotifLen\tMotif\tRefLen\tAllele1Len\tAllele2Len\tAlleles\tGenotype\tCovEnclosing\tCovFRR\tCovSpanning\tCovFlanking\tAllele1CI\tAllele2CI\tQualityScore\tFilterPass\n'
    header_written = 0

    for filename in os.listdir(vcf_files_path):
        if filename.endswith(".vcf"): 
            sample_name = filename.rsplit('.', 2)[0]
            file_vcf = os.path.join(vcf_files_path, filename) # VCF file location
            file_out = output_files_path + sample_name + '.txt' # Output file location
            file_combined = output_files_path + 'results_all.txt' # Output file location for combined file

            # Write files or output data on screen
            print("Working on the " + sample_name + ' sample.')

            if writefile == "combine": # Write all data into one file
                with open(file_combined, 'a') as output:
                    if header_written != 1:
                        output.write(header)
                    header_written = 1
                    get_strs(file_vcf, output, sample_name)
            elif writefile != "combine" and writefile is not None: # Write results into each sample's file
                with open(file_out, 'w') as output:
                    output.write(header)
                    get_strs(file_vcf, output, sample_name)
            else: # Output results on screen without writing into a file
                output = None
                print(header)
                get_strs(file_vcf, output, sample_name)
        else:
            continue
        
if __name__ == '__main__':
    main()
