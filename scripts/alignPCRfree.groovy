// Adjust parameters
def PLATFORM = 'ILLUMINA'
def REFERENCE = './reference/hg19.fa'

// Number of threads to use for BWA
threads = 16

align = {
	def fastaname = get_fname(REFERENCE)
		from('fastq.gz', 'fastq.gz') produce(get_fname(input.gz).prefix.prefix + '.bam') {
			exec """
				bwa mem -M -t $threads -R "@RG\\tID:NA12878\\tSM:NA12878" $REFERENCE $inputs | samtools view -h -S -b > $output.bam
			""", "bwamem"
	}
}
sort = {
	exec """
		samtools sort -m 16G -o $output.bam $input.bam
	""", "bwamem"
}

rmdup = {
    exec """
	    gatk MarkDuplicatesSpark \
	    -I $input.bam \
	    -O $output.bam \
	    --remove-sequencing-duplicates
    """, "picard"
}

index = {
	exec """
		samtools index $input.bam
	""", "picard"
}

run {
	align + sort + rmdup + index
}